var ContentHandler = require('./content'),
    ErrorHandler = require('./error').errorHandler;

module.exports = exports = function(app, db) {

    var contentHandler = new ContentHandler(db);

    app.get('/', contentHandler.displayMainPage);
    app.get('/webcam', contentHandler.displayWebcamPage);

    app.use(ErrorHandler);
}
