function ContentHandler(db) {
    "use strict";

    this.displayMainPage = function(req, res, next) {
        "use strict";

        return res.render('index', {
            title: "Main Page"
        });
    };

    this.displayWebcamPage = function(req, res, next) {
        "use strict";

        return res.render('webcam', {
            title: 'Webcam Homepage',
        });
    };
}

module.exports = ContentHandler;