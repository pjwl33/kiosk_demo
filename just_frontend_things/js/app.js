window.addEventListener('DOMContentLoaded', function() {
  // console.log('loaded bro');
  var ALPHA_NUMERICAL = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'SPACE', '_', '-', '.', '@'];
  var CAPS = _.map(ALPHA_NUMERICAL, function(val) {
    return typeof val === 'string' ? val.toUpperCase() : val;
  });
  var searchLetterBox = document.querySelector('div.search-letter-box'),
      letterBoxOptions = document.querySelector('div.letter-box-options'),
      searchResults = document.querySelector('div.search-results-box'),
      queryString = document.querySelector('div#query-string');

  function makeLetterBoxes(array) {
    _.each($('.letter-box'), function(el) {
      if (el.className.indexOf('btn-options') < 0) {
        el.remove();
      }
    });
    _.each(array, function(val) {
      var letterBox = document.createElement('DIV');
      letterBox.className = 'letter-box';
      letterBox.addEventListener('click', function() {
        string = this.innerHTML === 'SPACE' ? ' ' : this.innerHTML;
        queryString.innerHTML = (queryString.innerHTML += string);
      });
      letterBox.innerHTML = val;
      searchLetterBox.appendChild(letterBox);
    });
  }
  makeLetterBoxes(ALPHA_NUMERICAL);

  var shiftBtn = document.createElement('DIV');
  var shiftCounter = 0;
  shiftBtn.className = 'letter-box btn-options';
  shiftBtn.innerHTML = "SHIFT";
  shiftBtn.addEventListener('click', function() {
    console.log('all letters caps for 1 click');
  });
  letterBoxOptions.appendChild(shiftBtn);

  var capsBtn = document.createElement('DIV');
  var capsCounter = 0;
  capsBtn.className = 'letter-box btn-options';
  capsBtn.innerHTML = "CAPS LOCK";
  capsBtn.addEventListener('click', function() {
    var array = capsCounter % 2 === 0 ? CAPS : ALPHA_NUMERICAL;
    capsCounter += 1;
    makeLetterBoxes(array);
  });
  letterBoxOptions.appendChild(capsBtn);

});