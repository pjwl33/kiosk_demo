(function() {
	var container = document.querySelector('div.container'),
		// boxes
		first = document.getElementById('first'),
		second = document.getElementById('second'),
		third = document.getElementById('third'),
		fourth = document.getElementById('fourth'),
		// overlays
		overlay = document.getElementById('overlay-first'),
		overlaySecond = document.getElementById('overlay-second'),
		overlayThird = document.getElementById('overlay-third'),
		overlayFourth = document.getElementById('overlay-fourth'),
		// close buttons
		closeBttn = overlay.querySelector('button.overlay-close'),
		closeBttnSecond = overlaySecond.querySelector('button.overlay-close'),
		closeBttnThird = overlayThird.querySelector('button.overlay-close'),
		closeBttnFourth = overlayFourth.querySelector('button.overlay-close'),
		// loading animations
		loading = document.getElementById('loading-first'),
		loadingSecond = document.getElementById('loading-second'),
		loadingThird = document.getElementById('loading-third'),
		loadingFourth = document.getElementById('loading-fourth');
	// Confirm Info after scan
	// confirmInfoBttns = document.getElementsByClassName('btn-overlay');

	var allDemElementz = [{
		box: first,
		overlay: overlay,
		close: closeBttn,
		loading: loading
	}, {
		box: second,
		overlay: overlaySecond,
		close: closeBttnSecond,
		loading: loadingSecond
	}, {
		box: third,
		overlay: overlayThird,
		close: closeBttnThird,
		loading: loadingThird
	}, {
		box: fourth,
		overlay: overlayFourth,
		close: closeBttnFourth,
		loading: loadingFourth
	}];

	var transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
		support = {
			transitions: Modernizr.csstransitions
		};

	function toggleOverlay(overlay) {
		if (classie.has(overlay, 'open')) {
			classie.remove(overlay, 'open');
			classie.remove(container, 'overlay-open');
			classie.add(overlay, 'close');
			var onEndTransitionFn = function(ev) {
				if (support.transitions) {
					if (ev.propertyName !== 'visibility') return;
					this.removeEventListener(transEndEventName, onEndTransitionFn);
				}
				classie.remove(overlay, 'close');
			};
			if (support.transitions) {
				overlay.addEventListener(transEndEventName, onEndTransitionFn);
			} else {
				onEndTransitionFn();
			}
		} else if (!classie.has(overlay, 'close')) {
			classie.add(overlay, 'open');
			classie.add(container, 'overlay-open');
		}
	}

	_.each(allDemElementz, function(set) {
		set.box.addEventListener('click', function() {
			set.loading.style.visibility = 'visible';
			setTimeout(function() {
				toggleOverlay(set.overlay);
			}, 1800);
		});
		set.close.addEventListener('click', function() {
			set.loading.style.visibility = 'hidden';
			toggleOverlay(set.overlay);
		});
	});

	[].slice.call(document.querySelectorAll('.progress-button')).forEach(function(bttn, pos) {
		new UIProgressButton(bttn, {
			callback: function(instance) {
				var progress = 0,
					interval = setInterval(function() {
						progress = Math.min(progress + Math.random() * 0.1, 1);
						instance.setProgress(progress);

						// random at this point
						choice = _.sample([1, -1]);

						if (progress === 1) {
							instance.stop(choice);
							clearInterval(interval);
							if (choice === 1) {
								setTimeout(function() {
									location.href = "search.html";
								}, 850);
							}
						}
					}, 150);
			}
		});
	});
})();