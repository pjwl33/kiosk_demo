// var canvas = document.getElementById('testCanvas');
// var context = canvas.getContext('2d');

// LINES
// context.beginPath();
// context.moveTo(0, 0);
// context.lineTo(canvas.width / 2, canvas.height / 2);
// context.lineWidth = 20;
// context.strokeStyle = "#ddd";
// context.stroke();

// setTimeout(function() {
//   context.lineTo(canvas.width, canvas.height);
//   context.stroke();
// }, 2500);

// ARCS
// var endAngleValue = 0.00,
//     colorValue = 'orange';

// setInterval(function() {
//   if (endAngleValue > 1.60) {
//     endAngleValue = 0.00;
//     colorValue = colorValue == 'orange' ? 'blue' : 'orange';
//   }

//   var x = canvas.width / 2,
//       y = canvas.height / 2,
//       radius = 175,
//       // points of circle: {'right': 0 PI, 'bottom': 0.5 PI, 'left': 1 PI, 'top': 1.5 PI}, x and y are center of circle starting point
//       startAngle = 1.50 * Math.PI,
//       endAngle = endAngleValue * Math.PI,
//       counterClockwise = false;

//   context.beginPath();
//   context.lineWidth = 15;
//   context.strokeStyle = colorValue;

//   endAngleValue += 0.05;
//   context.arc(x, y, radius, startAngle, endAngle, counterClockwise);
//   context.stroke();
// }, 50);

// context.beginPath();
// context.moveTo(canvas.width / 2, canvas.height / 2);
// context.quadraticCurveTo(288, 0, 388, 150);
// context.lineWidth = 10;

// context.strokeStyle = "purple";
// context.stroke();

// EASEL JS

// function init() {
//   var canvas = document.getElementById('testCanvas');
//   var stage = new createjs.Stage(canvas);
//   var background = new createjs.Bitmap("images/logo.png");

//   stage.addChild(background);
//   stage.update();
// }

function init() {
  var canvas = document.getElementById('testCanvas');
  var stage = new createjs.Stage(canvas);
  var background = new createjs.Bitmap("images/background.png");
  var circle = new createjs.Shape();
  var circle2 = new createjs.Shape();
  var circle3 = new createjs.Shape();
  var circle4 = new createjs.Shape();

  circle.graphics.beginFill('blue').drawCircle(0, 0, 50);
  circle2.graphics.beginFill('yellow').drawCircle(0, 0, 50);
  circle3.graphics.beginFill('red').drawCircle(0, 0, 50);
  circle4.graphics.beginFill('green').drawCircle(0, 0, 50);
  //Set position of Shape instance.
  circle.x = circle.y = 0;
  circle3.x = circle3.y = 0;
  circle2.x = stage.canvas.width;
  circle2.y = stage.canvas.height;
  circle4.x = stage.canvas.width;
  circle4.y = stage.canvas.height;
  //Add Shape instance to stage display list.
  stage.addChild(circle);
  stage.addChild(circle2);
  stage.addChild(circle3);
  stage.addChild(circle4);
  stage.addChild(background);
  //Update stage will render next frame
  stage.update();

  // circle.addEventListener('click', handleClick);

  // function handleClick(event) {
  //   console.log('clicked!');
  // }

  // createjs.Ticker.addEventListener('tick', handleTick);

  // function handleTick() {
  //   circle.x += 15;
  //   circle2.x -= 15;
  //   circle3.y += 15;
  //   circle4.y -= 15;

  //   // 1
  //   if (circle.x > stage.canvas.width) {
  //     circle.x = 0;
  //     circle.y += 100;
  //   }
  //   if (circle.y > stage.canvas.height) {
  //     circle.y = 0;
  //   }

  //   // 2
  //   if (circle2.x <= 0) {
  //     circle2.x = stage.canvas.width;
  //     circle2.y -= 100;
  //   }
  //   if (circle2.y <= 0) {
  //     circle2.y = stage.canvas.height;
  //   }

  //   // 3
  //   if (circle3.y > stage.canvas.height) {
  //     circle3.x += 100;
  //     circle3.y = 0;
  //   }
  //   if (circle3.x > stage.canvas.width) {
  //     circle3.x = 0;
  //   }

  //   // 4
  //   if (circle4.y <= 0) {
  //     circle4.x -= 100;
  //     circle4.y = stage.canvas.height;
  //   }
  //   if (circle4.x <= 0) {
  //     circle4.x = stage.canvas.width;
  //   }

  //   stage.update();
  // }
}

