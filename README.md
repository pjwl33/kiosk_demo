###KIOSK TABLET/MOBILE CORDOVA APP

- for the check in piece on mobile/table
- later, will have scheduling piece integrated (like the shortpath visual update that's existent on /shortpath branch visitor_update_2.0)
- timeline is to do in next month (around end of Feb '15)
- want to show off at Las Vegas tradeshow in April '15
- accounts for Food & Drug, National Cancer Institute

- workflow for PIV-card holders and non-PIV-card holders
- non PIV card holders validate the license
- PIV-card holders don't need license validation

- if they are not scheduled how can they use the kiosk to check in?
- perhaps use twilio to make a phone call, to hit a button on that phone and then authorize that person on click

- can have tenant give authorization via phone number presses using twilio and their comp voice
- "press 1 to allow entry, 2 is they need escort, 5 to call them" - and will call at the kiosk, maybe even facetime

#####RIGHT NOW
- tenant needs to have approved visitor
- visitor checks in with guard
- guard prints out badge and allows entry if visitor is approved by tenant in system

#####POTENTIAL SOLUTION
- PIV and TWIT card credentials, maybe give them auth to unlock doors to get to host
- passport/driver license, id 150, powered by Assurtec
- they can input their own info
- twilio to host if they do not have (SMS)

- kiosk based on windows machine to start
- assuretec on ipad?

#####USER STORIES
* as a user i want to scan my barcode, DL, passport, or PIV card
* as a user, once I'm validated from the first step, I want to check to see if I am a scheduled visitor
* as a user, i want to search the tenants and respective visits scheduled and select my visit time
* as a user, i want to be confirmed by the tenant of my visit (via call)
* as a user, i want to then take a photo of myself for the badge, unless there were any problems with the call
* as a user, i want to take the badge and go to my visit